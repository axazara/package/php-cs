<?php

declare(strict_types=1);

namespace AxaZara\CS;

return [
    'declare_strict_types'         => true,
    'void_return'                  => true,
    'strict_param'                 => true,
];
